# SquirrelcamTiny85

ATTiny85 code for the squirrel cam.  You may ask why am I using an ATtiny85 to run a photoelectric circuit?  The same reason you use a Rolex to stop a wobbling table -- it's just the right thickness.