#include <EEPROM.h>
/********************************************************
 * This code is for a squirrel cam light sensor, found at 
 * https://www.hackster.io/reichley/solar-powered-squirrel-kam-pi-zero-w-updated-797db4
 * 
 * Originally I tried to get the lux sensor going, but the I2C libraries
 * for ATTiny85 were "difficult" to use at best.  Among other problems, 
 * Wire.h doesn't even compile for the ATTiny85.  Ultimately I went with a
 * light sensitive resistor (https://www.sparkfun.com/products/9088) and a
 * 10K resistor as shown at https://quadmeup.com/attiny85-light-sensor-i2c-slave-device/
 * I also borrowed heavily from the quadmeup.com page.
 * Later I plan to use i2c to provide sensor data.  
 ***************************************************/

/*
 * How often measurement should be executed
 * One tick is more less 0.5s, so 1 minute is 120 ticks
 * This executes measurements every 2 minutes, so 120s, 120000 ms
 * From quadmeup: #define MAX_TICK 120000 //2 minutes
 *
 * At this point, what's the harm in doing every second?  If/when 
 * I implement the sleep function, then it'll probably be worth sleeping 
 * for 2 min at a time, but for now, we'll just go every second.
 */
#define MAX_TICK 1000 

//#define SIXTY_SECONDS 6000 // Six seconds for now
#define SIXTY_SECONDS 60000 
#define TWIXT_CAL_INTERVAL 5000

#define STATUS_PIN_1 4
#define ADC_PIN A3

#define LPF_FACTOR 0.5

#define calibratePin 4
#define redPin 2
#define greenPin 1
#define boardPin 0
#define ON_DELAY 30
#define OFF_DELAY 250
#define NUM_SPACE 50

//#define TWILIGHT 650
#define TWILIGHT 500

#define SENSOR_PIN 3

#define FALSE 0
#define TRUE 1

#define EEPROM_START 0

int pins[] = {greenPin, redPin};
int num_pins = sizeof(pins)/sizeof(int);

int signalError(int count){
    int i;
    int period = 10;
    int errorPin = calibratePin;
    allOff();
    digitalWrite(errorPin, LOW);
    for (i = 0; i < count; i++){
        digitalWrite(errorPin, HIGH);
        delay(period);
        digitalWrite(errorPin, HIGH);
        delay(period);
    }
}

int signalSuccess(int count){
    int i;
    int period = 25;
    digitalWrite(boardPin, LOW);
    for (i = 0; i < count; i++){
        allOn(200);
        allOff();
    }
}

int initPins(int pins[]){
    int i;
    for (i = 0; i < num_pins; i++){
        pinMode(pins[i], OUTPUT);
        digitalWrite(pins[i], LOW);
    }
}

int blinkApin(int pin) {
    digitalWrite(pin, HIGH);
    delay(ON_DELAY);
    digitalWrite(pin, LOW);
    delay(OFF_DELAY);
}

int multiBlink(int pin, int count){
    int i;
    if (count > 20){
        count = 20;
    }
    for (i=0; i < count; i++) {
        blinkApin(pin);
    }
}

int allBlinkOnce(int period){
    int i;
    for (i = 0; i < num_pins; i++)
        digitalWrite(pins[i], HIGH);
    delay(period);
    for (i = 0; i < num_pins; i++)
        digitalWrite(pins[i], LOW);
}
int allFlicker(int count){
    int i;
    int period = 4;
    for (i = 0; i < count; i++){
        allOn(50);
        delay(period);
        allOff();
        delay(period);
    }
}

int blinkAcount(int count){
    int ones, tens, hundreds;
    int ones_pin = calibratePin;
    int tens_pin = redPin;
    int hundreds_pin = greenPin;
    int remainder;

    remainder = count;
    hundreds = remainder / 100;
    remainder = remainder % 100;
    tens = remainder / 10;
    remainder = remainder % 10;
    ones = remainder;
    if (hundreds < 0 || hundreds > 9){
        signalError(1000);
    }
    if (tens < 0 || tens > 9){
        signalError(100);
    }
    if (ones < 0 || ones > 9){
        signalError(10);
    }

    allOn(200);
    delay(250);
    allOff();
    delay(250);
    allOn(200);
    delay(1000);
    allOff();
    delay(250);
    multiBlink(hundreds_pin, hundreds);
    allOff();
    delay(250);
    multiBlink(tens_pin, tens);
    allOff();
    delay(250);
    multiBlink(ones_pin, ones);
    allOff();
}

int allOn(int ondelay){
    int i;
    delay(ondelay);
    for (i = 0; i < num_pins; i++)
        digitalWrite(pins[i], HIGH);
}
int allOff(){
    int i;
    for (i = 0; i < num_pins; i++)
        digitalWrite(pins[i], LOW);
}

int smooth(int data, float filterVal, float smoothedVal){

  if (filterVal > 1){      // check to make sure params are within range
    filterVal = .99;
  }
  else if (filterVal <= 0){
    filterVal = 0;
  }

  smoothedVal = (data * (1 - filterVal)) + (smoothedVal  *  filterVal);

  return (int)smoothedVal;
}

unsigned int tick = 0;
unsigned long lastReadout = 0;
volatile unsigned int lightMeter;
int in_shutdown = FALSE; 
unsigned long shutdown_start_time; 
unsigned int calibrate;

unsigned int light_threshold = 0;
unsigned long calibrate_millis = 0;

void setup() {
    initPins(pins);
    pinMode(ADC_PIN, INPUT);
    pinMode(calibratePin, INPUT_PULLUP);
    //multiBlink(greenPin, 5); //This is now the raspberry pi power cut. Don't blink it.
    multiBlink(redPin, 5);
    multiBlink(boardPin, 5);
    EEPROM.get(EEPROM_START, light_threshold);
    multiBlink(redPin, (int)(light_threshold/100));
    multiBlink(boardPin, 5);
    if (light_threshold == 0){
        light_threshold = TWILIGHT;
    }
}


unsigned int still_pressed = 0;
void loop() {
    unsigned long currentMillis = millis();

    calibrate = digitalRead(calibratePin);
    if (calibrate == LOW){
        if (calibrate_millis < (currentMillis - TWIXT_CAL_INTERVAL)){
            still_pressed = 1;
            light_threshold = analogRead(ADC_PIN);
            multiBlink(redPin, (int)(light_threshold/100));
            EEPROM.put(EEPROM_START, light_threshold);
            digitalWrite(boardPin, HIGH);
            delay(100);
            calibrate_millis = currentMillis;
        }
        /*
        else{
            if (!still_pressed)
                allFlicker(50);
        }
        */
    } else {
        still_pressed = 0;
        /* On tick value 0, do measurements;
         */
        if (abs(currentMillis - lastReadout) > MAX_TICK) {
            unsigned int raw_value = analogRead(ADC_PIN);
            
            //lightMeter = smooth(raw_value, LPF_FACTOR, lightMeter);
            lightMeter = raw_value;
            lastReadout = currentMillis;
            /* This allows some time for an orderly shutdown on the part of the 
             * raspberry pi, which will be monitoring the redPin ever second.  When it's 
             * high, run an orderly shutdown.
             */
            if (lightMeter < light_threshold){
                if (!in_shutdown){
                    shutdown_start_time = currentMillis;
                    in_shutdown = TRUE;
                    digitalWrite(redPin, HIGH);
                }
                if (currentMillis - shutdown_start_time > SIXTY_SECONDS){
                    digitalWrite(redPin, LOW);  // No pins should be high at this point.
                    digitalWrite(greenPin, LOW);
                } 
                /*
                else {
                    printf("currentMillis - shutdown_start_time: %ld > %d\n", 
                            (currentMillis - shutdown_start_time), SIXTY_SECONDS);
                }
                */
            } else {
                in_shutdown = FALSE;
                digitalWrite(redPin, LOW);
                digitalWrite(greenPin, HIGH);
                blinkApin(boardPin); // Only blink when we have the power 
            }
        }
    }
}
