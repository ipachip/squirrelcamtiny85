#!/usr/bin/env python3
""" See https://www.raspberrypi-spy.co.uk/2015/10/how-to-autorun-a-python-script-on-boot-using-systemd/
    for service installation instructions.
"""
import smtplib
from datetime import datetime as t
import RPi.GPIO as GPIO
import os
import time
import subprocess
import sys
from multiprocessing import Process

GPIO_PIN = 27
MAX_DAY_HOURS = "17:27"
HOURS, MIN = MAX_DAY_HOURS.split(':')
DAY_LEN = 3600 * HOURS + 60 * MIN

# listen on pi GPIO 27 for high signal
GPIO.setmode(GPIO.BCM)
GPIO.setup(GPIO_PIN, GPIO.IN)

# keeps track of system timestamp, meant to hold squirrelkam ops to 8 hours
workday = '/home/pi/workday.txt'

# email info, if you want to receive wakeup and shutdown notifications.
MAIL_USER = 'emailaddress'
MAIL_PASS = 'emailpassword'
SMTP_SERVER = 'emailserver' #usually smtp.xxxx.com
MAIL_RECIPIENT = 'emailrecipient'
SMTP_PORT = 587 # may be a different port, gmail uses 465

DEBUG = False

def send_email(recipient, subject, text):
    """    Sends an email when provided a recipient email address, subject, and text.
    """
    if MAIL_USER != 'emailaddress':
        server = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
        server.ehlo()
        server.starttls()
        server.ehlo()
        server.login(MAIL_USER, MAIL_PASS)
        header = 'To:' + recipient + '\n' + 'From: ' + MAIL_USER
        header = header + '\n' + 'Subject:' + subject + '\n'
        msg = header + '\n' + text + ' \n\n'
        # server.set_debuglevel(1)
        server.sendmail(MAIL_USER, recipient, msg)
        server.close()
    else:
        pass


def workday_start():
    """Writes system start time to file. If exists and < 8 hours old, passes. If >, overwrites."""
    if os.path.isfile(workday):
        if (time.time() - os.stat(workday).st_ctime < DAY_LEN):
            print('#shutdown_workday.py# file exists and less than 8 hours old.')
            pass
        else:
            with open(workday, 'w') as f:
                f.write(str(time.time()))
                print('#shutdown_workday.py# file existed but was 8 hours old, overwriting.')
    else:
        with open(workday, 'w') as f2:
            print('#shutdown_workday.py# file didn\'t exist so was created.')
            f2.write(str(time.time()))


def shutdown_check():
    """ Checks for HIGH signal on GPIO GPIO_PIN. This indicates light level
        has dropped below the calibrated value.  This function waits for 20
        seconds of continuous low-light signal.  This is intended to prevent a
        shutdown if, say, a shadow falls across the sensor.
    """ 
    dark_count = 0
    while True:
        if GPIO.input(GPIO_PIN) == 1:
            dark_count += 1
            if dark_count > 20:
                print('#shutdown_workday.py# squirrelhaus going to sleep: ({:})'.format(t.now().strftime('%d-%b-%y %H:%M')))
                send_email(MAIL_RECIPIENT, 
                           'squirrelhaus sleep', 
                           'squirrelhaus is going to sleep. ({:})'.format(t.now().strftime('%d-%b-%y %H:%M')))
                if DEBUG:
                    print("Pin " + str(GPIO_PIN) + " is high, exiting")
                else:
                    with open(os.devnull, 'w') as too_dark:
                        powerdownpi2 = subprocess.Popen(('sudo', 'poweroff',), stdout=too_dark)
                    sys.exit(0)
            else:
                if DEBUG:
                    print("Pin " + str(GPIO_PIN) + " is high, count is " + str(dark_count))
        else:
            dark_count = 0
        time.sleep(1.00)


def workday_over():
    """ Checks to see if difference between current time and time in file > DAY_LEN hours. If yes, shut down.
        This is more of a safety in case the signal of low light never comes.
    """
    while True:
        with open(workday, 'r') as g:
            time_card = float(g.read())
        if (time.time() - time_card) > DAY_LEN:
            print('#shutdown_workday.py# squirrelhaus has completed 8 hours of work: ({:})'.format(t.now().strftime('%d-%b-%y %H:%M')))
            send_email(MAIL_RECIPIENT, 
                       'squirrelhaus clocking out', 
                       'squirrelhaus has completed 8 hours of work. ({:})'.format(t.now().strftime('%d-%b-%y %H:%M')))
            with open(os.devnull, 'w') as day_over:
                powerdownpi1 = subprocess.Popen(('sudo', 'poweroff',), stdout=day_over)
            os.remove(workday)
            sys.exit(0)
        else:
            pass
        time.sleep(180)


if __name__ == "__main__":
    send_email(MAIL_RECIPIENT, 'squirrelhaus wake', 'squirrelhaus just woke up. ({:})'.format(t.now().strftime('%d-%b-%y %H:%M')))
    print('#shutdown_workday.py# squirrelhaus just woke up: ({:})'.format(t.now().strftime('%d-%b-%y %H:%M')))
    workday_start()
    process1 = Process(target = shutdown_check)
    process1.start()
    process2 = Process(target = workday_over)
    process2.start()

